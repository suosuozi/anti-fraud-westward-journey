# 反诈西游

## 项目介绍
多媒体大赛项目反诈西游

## 软件架构
软件架构说明

![architecture](README.assets/architecture.jpg)

## 目录结构说明

> ```
> anti-fraud-westward-journey
> ```
>
> > `.gitignore` -- 忽略配置
> >
> > `README.md` -- 自述文件
> >
> > `documents` -- 环境搭建、编码规范、项目需求等等文档资源
> >
> > `project-andorid` -- `andorid`项目主体
> >
> > `project-front` -- 前端项目主体
> >
> > `project-java` -- `Java`项目主体

## 软件架构

### `Java`后端核心技术栈

版本匹配参考：

[https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba%2Fspring-cloud-alibaba%2Fwiki%2F%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

| 技术                     | 说明                   | 版本          | 备注                                                         |
| ------------------------ | ---------------------- | ------------- | ------------------------------------------------------------ |
| `Spring`                 | 容器                   | 5.2.15        | [https://spring.io/](https://gitee.com/link?target=https%3A%2F%2Fspring.io%2F) |
| `Spring Web MVC`         | `MVC`框架              | 5.2.15        | [https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html](https://gitee.com/link?target=https%3A%2F%2Fdocs.spring.io%2Fspring%2Fdocs%2Fcurrent%2Fspring-framework-reference%2Fweb.html) |
| `Beanvalidation`         | 实体属性校验           | 2.0.2         | [https://beanvalidation.org/2.0-jsr380/](https://gitee.com/link?target=https%3A%2F%2Fbeanvalidation.org%2F2.0-jsr380%2F) [https://www.baeldung.com/spring-boot-bean-validation](https://gitee.com/link?target=https%3A%2F%2Fwww.baeldung.com%2Fspring-boot-bean-validation) |
| `MyBatis`                | `ORM`框架              | 3.5.7         | [http://www.mybatis.org/mybatis-3/zh/index.html](https://gitee.com/link?target=http%3A%2F%2Fwww.mybatis.org%2Fmybatis-3%2Fzh%2Findex.html) |
| `MyBatis Plus`           | `MyBatis`的增强工具    | 3.4.3.4       | [https://baomidou.com/](https://gitee.com/link?target=https%3A%2F%2Fbaomidou.com%2F) |
| `MyBatis Plus Generator` | 代码生成器             | 3.5.1         | [https://github.com/baomidou/generator](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fbaomidou%2Fgenerator) |
| `Druid`                  | 数据库连接池           | 1.2.8         | [https://github.com/alibaba/druid](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba%2Fdruid) |
| `Lombok`                 | 实体类增加工具         | 1.18.20       | [https://github.com/rzwitserloot/lombok](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Frzwitserloot%2Flombok) |
| `Hutool`                 | Java工具类库           | 5.7.16        | [https://hutool.cn/docs/#/](https://gitee.com/link?target=https%3A%2F%2Fhutool.cn%2Fdocs%2F%23%2F) |
| `Knife4j`                | 接口描述语言           | 3.0.3         | https://gitee.com/xiaoym/knife4j                             |
| `Nimbus JOSE JWT`        | `JSON Web Token`       | 8.21          | [https://bitbucket.org/connect2id/nimbus-jose-jwt/wiki/Home](https://gitee.com/link?target=https%3A%2F%2Fbitbucket.org%2Fconnect2id%2Fnimbus-jose-jwt%2Fwiki%2FHome) |
| `Jedis`                  | `Redis Java`客户端     | 3.7.0         | [https://github.com/redis/jedis](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fredis%2Fjedis) |
| `Spring Boot`            | Spring快速集成脚手架   | 2.3.12        | [https://spring.io/projects/spring-boot](https://gitee.com/link?target=https%3A%2F%2Fspring.io%2Fprojects%2Fspring-boot) |
| `Spring Cloud`           | 微服务框架             | `Hoxton.SR12` | [https://spring.io/projects/spring-cloud](https://gitee.com/link?target=https%3A%2F%2Fspring.io%2Fprojects%2Fspring-cloud) |
| `Spring Cloud Alibaba`   | 微服务框架             | 2.2.7         | [https://github.com/alibaba/spring-cloud-alibaba/wiki](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba%2Fspring-cloud-alibaba%2Fwiki) |
| `Spring Cloud Security`  | 认证和授权框架         | 2.2.5         | [https://spring.io/projects/spring-cloud-security](https://gitee.com/link?target=https%3A%2F%2Fspring.io%2Fprojects%2Fspring-cloud-security) |
| `Sentinel`               | 分布式系统的流量防卫兵 | 1.8.1         | [https://sentinelguard.io/zh-cn/](https://gitee.com/link?target=https%3A%2F%2Fsentinelguard.io%2Fzh-cn%2F) |
| `Seata`                  | 分布式事务解决方案     | 1.3.0         | [https://seata.io/zh-cn/](https://gitee.com/link?target=https%3A%2F%2Fseata.io%2Fzh-cn%2F) |

### `Java`后端扩展技术栈

| 技术               | 说明           | 版本   | 备注                                                         |
| ------------------ | -------------- | ------ | ------------------------------------------------------------ |
| `easyexcel`        | Excel报表      | 3.0.5  | [https://github.com/alibaba/easyexcel](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba%2Feasyexcel) |
| `RocketMQ`         | 事件驱动中间件 | 4.6.1  | [https://github.com/alibaba/spring-cloud-alibaba/wiki/RocketMQ](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba%2Fspring-cloud-alibaba%2Fwiki%2FRocketMQ) |
| `Spring WebSocket` | 及时通讯服务   | 5.2.15 | [https://www.baeldung.com/websockets-spring](https://gitee.com/link?target=https%3A%2F%2Fwww.baeldung.com%2Fwebsockets-spring) |
| `FastDFS`          | `dfs`客户端    | 1.29   | [https://bluemiaomiao.github.io/fastdfs-spring-boot-starter/](https://gitee.com/link?target=https%3A%2F%2Fbluemiaomiao.github.io%2Ffastdfs-spring-boot-starter%2F) |

### `Vue`前端技术栈

| 技术           | 说明             | 版本   | 备注                                    |
| -------------- | ---------------- | ------ | --------------------------------------- |
| `vue-cli`      | 项目构建工具     | 5.0.4  | https://cli.vuejs.org/                  |
| `vue-router`   | 前端路由工具     | 4.0.3  | https://router.vuejs.org/               |
| `vuex`         | 状态管理工具     | 4.0.0  | https://vuex.vuejs.org/                 |
| `Vue.js`       | 轻量级的MVVM框架 | 3.2.13 | https://cn.vuejs.org/index.html         |
| `Axios`        | http请求工具     | 0.27.2 | https://github.com/axios/axios          |
| `ES6`          | 简化Js代码       | 6      | https://www.w3schools.com/js/js_es6.asp |
| `element-plus` | 前端ui组件       | 2.2.5  | https://element-plus.gitee.io/zh-CN/    |
| `less`         | CSS扩展语言      | 4.1.3  | https://less.bootcss.com/               |

### 移动端技术栈

|      |      |      |      |
| ---- | ---- | ---- | ---- |
|      |      |      |      |
|      |      |      |      |
|      |      |      |      |
|      |      |      |      |
|      |      |      |      |
|      |      |      |      |
|      |      |      |      |

## 环境搭建

### 开发工具

| 工具            | 说明                  | 版本   | 备注                                                         |
| --------------- | --------------------- | ------ | ------------------------------------------------------------ |
| `IDEA`          | 开发`IDE`             | 2021.3 | [https://www.jetbrains.com/idea/download](https://gitee.com/link?target=https%3A%2F%2Fwww.jetbrains.com%2Fidea%2Fdownload) |
| `Navicat`       | 数据库连接工具        | latest | [https://www.navicat.com.cn/](https://gitee.com/link?target=https%3A%2F%2Fwww.navicat.com.cn%2F) |
| `RDM`           | `Redis`可视化管理工具 | latest | [https://github.com/uglide/RedisDesktopManager](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fuglide%2FRedisDesktopManager) https://gitee.com/qishibo/AnotherRedisDesktopManager |
| `PowerDesigner` | 数据库设计工具        | 16.6   | [http://powerdesigner.de/](https://gitee.com/link?target=http%3A%2F%2Fpowerdesigner.de%2F) |
| `Axure`         | 原型设计工具          | 9      | [https://www.axure.com/](https://gitee.com/link?target=https%3A%2F%2Fwww.axure.com%2F) |
| `MindMaster`    | 思维导图设计工具      | latest | [http://www.edrawsoft.cn/mindmaster](https://gitee.com/link?target=http%3A%2F%2Fwww.edrawsoft.cn%2Fmindmaster) |
| `Visio`         | 流程图绘制工具        | latest |                                                              |
| `Apipost`       | `API`接口调试工具     | latest | [https://www.apipost.cn/](https://gitee.com/link?target=https%3A%2F%2Fwww.apipost.cn%2F) |

### 开发环境

| 依赖环境  | 版本      | 备注                                                         |
| --------- | --------- | ------------------------------------------------------------ |
| `Windows` | 10        | 操作系统                                                     |
| `JDK`     | 1.8.0_311 | [https://www.injdk.cn/](https://gitee.com/link?target=https%3A%2F%2Fwww.injdk.cn%2F) |
| `NodeJS`  |           | [https://nodejs.org/zh-cn/](https://gitee.com/link?target=https%3A%2F%2Fnodejs.org%2Fzh-cn%2F) |

### 服务器环境

| 依赖环境   | 版本                                                         | 备注                                                         |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| `CentOS`   | 7                                                            | 操作系统（可用`VM`安装`Centos7`）                            |
| `Docker`   | latest                                                       | [https://www.docker.com/](https://gitee.com/link?target=https%3A%2F%2Fwww.docker.com%2F) |
| `MySQL`    | 8                                                            | [https://www.mysql.com/cn/](https://gitee.com/link?target=https%3A%2F%2Fwww.mysql.com%2Fcn%2F) |
| `Redis`    | 6.2.6                                                        | [https://redis.io/](https://gitee.com/link?target=https%3A%2F%2Fredis.io%2F) |
| `Nacos`    | 2.0.3                                                        | [https://nacos.io/zh-cn/docs/quick-start-docker.html](https://gitee.com/link?target=https%3A%2F%2Fnacos.io%2Fzh-cn%2Fdocs%2Fquick-start-docker.html) |
| `Sentinel` | 1.8.1                                                        | [https://github.com/alibaba/Sentinel/releases](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba%2FSentinel%2Freleases) |
| `Seata`    | 1.3.0                                                        | [https://github.com/seata/seata](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fseata%2Fseata) |
| `RocketMQ` | 4.6.1                                                        | [https://rocketmq.apache.org/](https://gitee.com/link?target=https%3A%2F%2Frocketmq.apache.org%2F) |
| `Nginx`    | latest                                                       | [https://nginx.org/en/](https://gitee.com/link?target=https%3A%2F%2Fnginx.org%2Fen%2F) |
| `FastDFS`  | [V6.07](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fhappyfish100%2Ffastdfs%2Freleases%2Ftag%2FV6.07) | https://gitee.com/fastdfs100                                 |

## 特别鸣谢

`anti-fraud-westward-journey`的诞生离不开开源软件的支持，感谢以下开源项目及项目维护者：

- spring：[https://github.com/spring-projects](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fspring-projects)
- alibaba：[https://github.com/alibaba](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Falibaba)
- mybatis：[https://github.com/mybatis/mybatis-3.git](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fmybatis%2Fmybatis-3.git)
- mp：[https://github.com/baomidou](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fbaomidou)
- api：https://gitee.com/xiaoym/knife4j
- `vue`：[https://github.com/vuejs](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fvuejs)
- ui：[https://github.com/ElemeFE](https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2FElemeFE)
